#!/usr/bin/python

import re 


expr = re.compile("[a-zA-Z]\w*\s:(\s[a-zA-Z]\w*,)*\s[a-zA-Z]\d*")

if expr.match("bonjour : plop"):
	print "bonjour :"
else:
	print "pas bonjour"
if expr.match('bonjour : plop, plopiplop'):
	print "bonjour:"
else:
	print " pas bonjour:"
