#!/usr/bin/python

from ClusterShell.NodeSet import NodeSet
from ClusterShell.Task import task_self

ns1 = NodeSet("node[1-2]")
print ns1

task = task_self()

task.run("touch /tmp/plop", nodes=ns1)


for i, ne in task.iter_buffers():
	print i, ne

