#! /usr/bin/python
#! /usr/bin/python
from ClusterShell.Task import task_self
from ClusterShell.NodeSet import NodeSet

class GestionLieux:
	def actionlieux(self,lieu):

		n=NodeSet()
		dico = {}
		liste_1= open("fichierliste.txt", "r")
		liste = liste_1.read().splitlines()
		liste_1.close()
		ma_liste = []

		for ligne in liste: #permet de mettre les donnes du fichier dans un dico
			element = ligne.split(":")
			cle = element[1].split(",")
			for c in cle:
				c = c.replace(' ','')
			dico[element[0].strip()] = cle 

		for i in dico.keys():
			if i == lieu:
				ma_liste.append(dico[i]) # Et on le rajoute dans une liste
		if not ma_liste:
			variable = input("Ville introuvable veuillez taper une autre ville : ")
			actionlieux(variable)

		for element in ma_liste[0]:# on mets les noeuds du 1er element dans la liste dans le node set
        		n.add(element)

		task=task_self()
		task.shell("service gdm3 status",nodes=n)
		task.resume()

		for buf, nodes in task.iter_buffers():
        		print nodes, buf

