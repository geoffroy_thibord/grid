#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
from logging.handlers import RotatingFileHandler
from ClusterShell.NodeSet import NodeSet
from ClusterShell.Task import task_self
import re
import copy

class Metier:
    """
    Classe metier fournissant tous les services de notre application
    Récupère les groupes de noeuds et de services ainsi que les liens de 
    dependance entre service depuis les fichiers de configuration spécifiés dans ses attributs
    Mets a disposition les groupes sous forme de dictionnaires
    Permet d'effectuer des opérations sur divers noeuds grâce a l'API python de clustershell
    """

    fichierNoeuds="groupesNoeuds.cfg"
    """
    fichier de configuration contenant les groupe de noeuds
    Format de chaque entrée: <groupe1>:<element1>,<element2>,[...],<elementN>
    """

    fichierServices="groupesServices.cfg"
    """
    fichier de configuration contenant les groupes de services
    Format de chaque entrée: <groupe1>:<element1>,<element2>,[...],<elementN>
    """

    fichierDependances="dependances.cfg"
    """
    fichier de configuration contenant les dependances
    Format de chaque entrée: <element1>:<element2>,<element3>,[...],<elementN>
    Où element 1 précède les éléments 2 à n
    """

    def __init__(self):
        """
        Initialise la classe Metier en lisant les 2 fichiers de configuration sue la classe a en attribut
        Les informations des fichiers sont stockes dans 2 dictionnaires
        Les lignes ne matchant pas le format voulu sont ignorees
        """
        
        # expression reguliere définissant le format souhaité pour chaque ligne de nos fichiers de configuration
        self.xpr = re.compile("[a-zA-Z]\w*\s*:(\s*[a-zA-Z]\w*,)*\s*[a-zA-Z]\d*")

        #initialise le loggeur
        self.initLoggeur()
        
        self.grpNoeuds={}
        self.grpServices={}

        # stocke les dependances au format clef : liste de valeur ou la clefs précède les valeurs de la liste en terme de dependances
        self.dependances={}

        
        # stocke les dependances au format clef : liste de valeur ou les valeurs de la liste précèdent la clef en terme de dependances
        self.reverseDependances={}

        # chargement du fichier de dependances
        self.chargerDependances()

        fichier = open(self.fichierNoeuds,"r")

        listeOrigin = fichier.read().splitlines()

        fichier.close()
        self.logger.info("lecture du fichier de groupes de noeuds") 
        
        #parcours des lignes du fichier de configuration
        for l in listeOrigin:
            
            # comparaison de notre ligne avec l'expression regulière definissant le format que l'on souhaite pour nos fichiers
            if self.xpr.match(l):
                #séparation du groupe des éléments grace au caractere ':' puis des éléments grace à ','
                entree=l.split(":")
                noeuds = entree[1].split(",")
                
                # suppression des espaces
                for n in noeuds:
                    n = n.replace(' ','')

                # groupe ajouté come clef du dictionnaire avec comme valeur la liste de éléments qui le composent
                self.grpNoeuds[entree[0].strip()]=noeuds

            else:
                #message d'erreur si la ligne ne match pas not regex
                self.logger.error("Erreur dans la ligne")

        self.logger.info("lecture du fichier de groupes de noeuds terminee")

        self.logger.debug(self.grpNoeuds)

        
        #le traitement est quasi identique pour la lecture du fichier des services
        fichier = open(self.fichierServices,"r")

        listeOrigin = fichier.read().splitlines()

        fichier.close()

        self.logger.info("lecture du fichier de groupes de services...")
        
        for l in listeOrigin:
            
            if self.xpr.match(l):
                entree=l.split(":")
                services = entree[1].split(",")
    
                for s in services:
                    s = s.replace(' ','')
                self.grpServices[entree[0].strip()]=services

            else:
                self.logger.error("Erreur dans la ligne")

        self.logger.info("lecture du fichier de groupes de services terminee")
        
        self.logger.debug(self.grpServices)


    def chargerDependances(self):
        """
        Charge le fichier de dependances et stocke les données dans un dictionnaire 
        """

        fichier = open(self.fichierDependances,"r")

        listeOrigin = fichier.read().splitlines()

        fichier.close()

        self.logger.info("lecture du fichier des dependances...")

        for l in listeOrigin:

            if self.xpr.match(l):
                entree=l.split(":")
                services = entree[1].split(",")

                for s in services:
                    s = s.replace(' ','')
                    # chaque service s précédé du service dans entree[0] est ajouté comme clef dans le dictionnaire reverse
                    # si l'entrér n'existe pas elle est alors crée avec la valeur par defaut de []
                    # à cela on ajoute entree[0]
                    self.reverseDependances.setdefault(s,[]).append(entree[0].strip())
                self.dependances[entree[0].strip()]=services

            else:
                self.logger.error("Erreur dans la ligne")

        self.logger.info("lecture du fichier des dependances terminée")

        self.logger.debug(self.dependances)
        self.logger.debug(self.reverseDependances)
        
        
    def initLoggeur(self):
        """
        initialise le loggeur
        Il est actuellement reglé pour accepter les messages a partir du niveau DEBUG
        """
        # création de l'objet logger qui va nous servir à écrire dans les logs
        self.logger = logging.getLogger()
        # on met le niveau du logger à DEBUG, comme ça il écrit tout
        self.logger.setLevel(logging.DEBUG)

        # création d'un formateur qui va ajouter le temps, le niveau
        # de chaque message quand on écrira un message dans le log
        formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
        # création d'un handler qui va rediriger une écriture du log vers
        # un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
        file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
        # on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
        # créé précédement et on ajoute ce handler au logger
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        # création d'un second handler qui va rediriger chaque écriture de log
        # sur la console
        steam_handler = logging.StreamHandler()
        steam_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(steam_handler)


    def operation(self, noeuds, service, action):
        """
        Effectue l'action specifiee dans action sur le service passe en parametre sur les noeuds specifies
        Si les noeuds sont un groupe de noeud (qui apparait dans le dictionnaire) alors operation se rappel sur chaque element du groupe
        Si le service est un groupe de service, la methode se rappellere pour chaque service du groupe
        ATTENTION: Cette methode ne prend pas les dependances en compte
        """
        
        if noeuds in self.grpNoeuds:
            for n in self.gprNoeuds[noeuds]:
                self.operation(n, service, action)
        elif service in self.grpServices:
            for s in self.grpServices[service]:
                self.operation(noeuds, s, action)
        else:
            ns=NodeSet()
            ns.add(noeuds)
            cmd= "service "+service+" "+action
            task=task_self()
            task.shell(cmd, nodes=ns)
            task.resume()
            for buff, nodes in task.iter_buffers():
                #print nodes, buff
                msg= nodes.__str__()+buff.__str__()
                self.logger.info(msg)

    def operationDep(self, noeuds, service, action):
        """
        Effectue l'operation sur les services spécifiés sur les noeuds spécifiés en prenant en compte les dependances
        """

        #tout d'abord l'ensemble de noeuds est aplatie de maniere a n'avoir qu'une liste de noeuds
        flattened=self.flattenNoeuds(noeuds)
        ns=NodeSet()

        # on ajoute tous ces noeuds au nodeset
        for n in flattened:
            ns.add(n) 

        # creation de l'objet gérant les tâches
        task=task_self()

        # Si l'on veut faire l'action start il nous faut tous les services dont dépend notre en ensemble de service a lancer
        if action=="start":
            # ordreServ reçois la liste des services dont dépendent les services dans flattened
            ordreServ=self.creerListeTopo(self.reverseDependances, service)
            ordreServ.reverse()
            for s in ordreServ:
                cmd="service "+s+" start"
                task.shell(cmd, nodes=ns)
            task.resume()
        elif action=="stop":
            ordreServ=self.creerListeTopo(self.dependances, service)
            ordreServ.reverse()
            for s in ordreServ:
                cmd="service "+s+" stop"
                task.shell(cmd, nodes=ns)
            task.resume()
        elif action=="restart":
            ordreServ=self.creerListeTopo(self.dependances, service)
            ordreServ.reverse()
            for s in ordreServ:
                cmd="service "+s+" stop"
                task.shell(cmd, nodes=ns)
            ordreServ.reverse()
            for s in ordreServ:
                cmd="service "+s+" start"
                task.shell(cmd, nodes=ns)
            task.resume()
 
        elif action=="status":
            cmd="service "+s+" status"
        else:
            raise NameError("argument action invalide")

        for buff, nodes in task.iter_buffers():
            #print nodes, buff
            msg= nodes.__str__()+buff.__str__()
            self.logger.info(msg) 

    def creerListeTopo(self, dicoDependances, service):
        """
        creer une liste topologique du service (ou groupe) passé en parametre et des services qui en dependent
        les 
        cette liste ne doit PAS inclure de groupe de service
        cette liste est faite à partir du dictionnaire des dependances paasé en parametre
        utiliser le dictionnaire reverse des dependances permet d'avoir une liste topologique inversée des services et de ceux dont ils dependent
        Implementation de l'algorithme de tri topologique de Tarjan qui explore le graphe en mode depth first
        """
        #work in progress!!!
        
        #liste dans laquelle on met les elements par ordre inverse du tri topologique puis que l'on renversera
        listeTopo=[]

        #dictionnaire avec le marquage de chacun des noeuds traités
        # le premier booleen est le marquage permanent disant que l'on a exploré tous les noeuds accessibles depuis celui-ci
        # le deuxieme marquage indique que notre chemin courant d'exploration en profondeur passe par ce noeud
        mark={}
        services=self.flattenService(service)
        print "services vaut", services
        for s in services:
            if not mark.setdefault(s, [False,False])[0]:
                self.visit(s,mark,listeTopo, dicoDependances)
        listeTopo.reverse()
        return listeTopo
    
    def visit(self, service, mark, listeTopo,dicoDependances):
        """
        Visite un noeud
        Fonction récursive utilisée par l'algorithme de CreerListeTopo
        Cette methode explore en profondeur le graphe à partir du noeud passé en paramètre
        """
        print "visite de" , service
        print "valeur de mark", mark
        # si l'on rencontre un service deja marqué temporairement c'est qu'il est dans notre chemin d'exploration en profondeur
        # on a donc bouclé et un graphe de dependances cyclique est incorrect
        if mark.setdefault(service,[False,False])[1]:
            raise NameError("boucle dans les dependances")
        if not mark[service][0]:
            mark[service][1]=True
            if service in dicoDependances:
                for s in dicoDependances[service]:
                    self.visit(s,mark,listeTopo,dicoDependances)
            mark[service][0]=True
            mark[service][1]=False
            listeTopo.append(service)

    def flattenService(self, service):
        """
        Donne une liste de services a partir d'un service/groupe de services
        """
        liste=[]
        if service in self.grpServices:
            for s in self.grpServices[service]:
                liste=liste+self.flattenService(s)
        else:
            liste.append(service)

        return list(set(liste))

    def flattenNoeuds(self, noeuds):
        """
        Donne une liste de noeuds a partir d'un noeud/groupe de noeuds
        """
        liste=[]
        if noeuds in self.grpNoeuds:
            for n in self.grpNoeuds[noeuds]:
                liste=liste+self.flattenNoeuds(n)
        else:
            liste.append(noeuds)
        return list(set(liste))

    def getGrpNodes(self):
        """
        Renvoi une COPIE du dictionnaire des noeuds de la classe
        """
        return copy.deepcopy(self.grpNoeuds)



    def getGrpServices(self):
        """
        Renvoi une COPIE du dictionnaire des services de la classe
        """
        return copy.deepcopy(self.grpServices)

    def getListeGrpNodes(self):
        """
        Renvoi une liste des clefs du dictionnaire des groupes de noeuds
        """
        liste=[]
        for key in self.grpNodes:
            liste.append(key)
        return liste

    def getListeGrpServices(self):
        """
        Renvoi une liste des clefs du dictionnaire des groupes de services
        """
        liste=[]
        for key in self.grpServices:
            liste.append(key)
        return liste

